# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Collaborative workspace for Keri Heuer and Sean Lewis
* Perform KS-tests on given star particle properties from Torch simulations: **do stellar mass distributions differ from [Kroupa IMF](https://arxiv.org/pdf/astro-ph/0201098.pdf) distribution?**

### Background/Hypothesis ###

In Torch we generate star particles by sampling from the Kroupa IMF distribution, randomizing the order in which we spawn stars and only spawning them once enough gas has been accreted to form the next star on the list.

In simulations where the GMC structure is violently disrupted, less gas is available to the outer regions of the cloud that fragment into small star clusters. Due to the lack of available/accretable gas, the resulting star clusters will not be able to appropriately sample the Kroupa IMF. This effect should be most noticible in the reduced number of high mass stars, as they would be the stars most likely to begin forming but never be spawned or never begin forming in the first place due to local gas being exhausted.

A KS-test will show to what degree the resulting cluster mass distribution diverges from the Kroupa IMF from which we sample all possible stars to form. 

### Repository Contents ###

* snapshot-analysis-py2.ipynb
	* An example notebook for how to load star cluster data, what data is present, etc. No need to worry about details in this notebook, meant mostly as a test. If you can run cells here, you're set up properly.
* energy_routines.py
	* A script of function definitions used elsewhere in other Torch analyses. In this we only care about the cluster_storage class object, as this is what is being loaded out of the pickle files.
* sean_run_data.tar.gz
	* Collection of pickled cluster_storage objects for snapshots of simulations taken at t = 2 x free-fall-time and t = 20% gas remaining.

### How do I get set up? ###

1. Clone this repository on your local machine.
2. Clone and install [AMUSE](https://github.com/amusecode/amuse)
3. Set local python path to this directory

Dependencies:

* python2.7 (due to non-backwards compatible .pickle files).
* [AMUSE](https://github.com/amusecode/amuse) software suite. Follow link, clone, and checkout python2 branch.

Python2.7 libraries

* numpy
* sklearn
* pickle

### Troubleshooting ###

* Make sure your $PYTHONPATH environmental variable points to this working directroy so that the cluster_storage object is imported correctly.
* Setting up AMUSE is notoriously difficult. Ask Sean or Dr. McMillan for tips or a walkthrough of the install procedure.

